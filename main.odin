package main

import "core:c"
import "core:time"
import "core:math"
import "core:sys/windows"
import sdl "vendor:sdl2"

// Kernel32 bindings
foreign import kernel32 "system:Kernel32.lib"
PFIBER_START_ROUTINE :: #type proc "stdcall" (lpFiberParameter: rawptr)
Fiber :: distinct rawptr
Thread :: distinct u32
@(default_calling_convention="stdcall")
foreign kernel32 {
    ConvertThreadToFiber :: proc(lpParameter: rawptr) -> Fiber ---
    ConvertFiberToThread :: proc() -> b32 ---
    CreateFiber :: proc(
        dwStackSize: c.size_t,
        lpStartAddress: PFIBER_START_ROUTINE,
        lpParameter: rawptr,
    ) -> Fiber ---
    DeleteFiber :: proc(lpFiber: Fiber) ---
    SwitchToFiber :: proc(lpFiber: Fiber) ---
    GetCurrentThreadId :: proc() -> Thread ---
}

TIMER_ID :: 1

Stuff :: struct {
    main_thread_id: Thread,
    main_fiber: Fiber,
    window: ^sdl.Window,
    should_quit: bool,
}

event_filter :: proc "c" (userdata: rawptr, event: ^sdl.Event) -> c.int {
    stuff := cast(^Stuff) userdata

    // If the event is not being received by the main thread, we don't do anything -- calling SwitchToFiber in this case would be bad.
    if GetCurrentThreadId() != stuff.main_thread_id do return 1

    if event.type == .SYSWMEVENT {
        winevent := event.syswm.msg.msg.win
        switch winevent.msg {
        // We want to handle any messages that imply that a modal loop would be running.
        case windows.WM_ENTERSIZEMOVE,
             windows.WM_ENTERMENULOOP,
             windows.WM_SIZE,
             windows.WM_MOVE:
            windows.SetTimer(windows.HWND(winevent.hwnd), TIMER_ID, 1, nil)
            SwitchToFiber(stuff.main_fiber)
        // Stop the timer when we exit the loop.
        case windows.WM_EXITSIZEMOVE,
             windows.WM_EXITMENULOOP:
            windows.KillTimer(get_hwnd(cast(^sdl.Window) stuff.window), TIMER_ID)
        // If we are in a modal loop and not receiving messages like SIZE or MOVE, the timer allows us to still return control to the main fiber regularly.
        case windows.WM_TIMER:
            if winevent.wParam == TIMER_ID {
                SwitchToFiber(stuff.main_fiber)
            }
        }
        return 0 // Don't add the event to the internal queue.
    }
    return 1
}

get_hwnd :: proc "contextless" (window: ^sdl.Window) -> windows.HWND {
    info: sdl.SysWMinfo
    sdl.GetWindowWMInfo(window, &info)
    return windows.HWND(info.info.win.window)
}

event_fiber_proc :: proc "stdcall" (lpParameter: rawptr) {
    stuff := cast(^Stuff) lpParameter
    for {
        event: sdl.Event
        for sdl.PollEvent(&event) != 0 {
            #partial switch event.type {
                case .QUIT:
                    stuff.should_quit = true
                case .KEYDOWN:
                    #partial switch event.key.keysym.sym {
                        case .ESCAPE: stuff.should_quit = true
                    }
            }
        }

        // We wouldn't want the timer to keep running at all times, so we always make sure to stop it once we are out of the event loop.
        windows.KillTimer(get_hwnd(cast(^sdl.Window) stuff.window), TIMER_ID)

        SwitchToFiber(stuff.main_fiber)
    }
}

main :: proc() {
    stuff: Stuff
    stuff.main_thread_id = GetCurrentThreadId()

    stuff.main_fiber = ConvertThreadToFiber(nil)
    defer ConvertFiberToThread()

    // The event fiber is responsible for running the event loop. This allows us to return control to the main fiber to update the window contents at any time in the loop.
    event_fiber := CreateFiber(0, event_fiber_proc, &stuff)
    defer DeleteFiber(event_fiber)

    sdl.Init({.VIDEO, .TIMER})
    defer sdl.Quit()

    renderer: ^sdl.Renderer
    sdl.CreateWindowAndRenderer(256, 256, {.RESIZABLE}, &stuff.window, &renderer)
    sdl.SetWindowTitle(stuff.window, "sdlwin")
    sdl.SetWindowMinimumSize(stuff.window, 100, 100)

    // Enabling SYSWMEVENT and using an event filter allows us to handle window messages as soon as SDL receives them, without needing to wait for the modal loop to end.
    sdl.EventState(.SYSWMEVENT, sdl.ENABLE)
    sdl.SetEventFilter(event_filter, &stuff)

    start_time := time.tick_now()

    for !stuff.should_quit {
        t := time.duration_seconds(time.tick_since(start_time))

        sdl.SetRenderDrawColor(renderer, u8(((math.sin(t*4.0)+1)/2)*255), u8(((math.sin(t*4.25)+1)/2)*255), u8(((math.sin(t*4.5)+1)/2)*255), 255)
        sdl.RenderClear(renderer)
        sdl.RenderPresent(renderer)

        // Run the event loop
        SwitchToFiber(event_fiber)
    }
}
